import { Component, OnInit, Input, OnChanges } from '@angular/core';
import { User } from '../../models/user.model';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss'],
})
export class TableComponent implements OnInit, OnChanges {
  @Input() users: User[];
  @Input() showPagenation: boolean;

  usersNotFound = false;
  usersCopy!: User[];
  noOfRecordsPerPage = [10, 20, 30, 50];
  paginationList = Array(5)
    .fill(0)
    .map((x, i) => i);
  numberOfRecords = 10;
  selectedPage = 0;
  totalPages = 0;
  disablePrev = true;
  disableNext = false;
  thisPage = 0;
  startIndex = 0;
  lastIndex = 0;
  recordsNotFound = false;

  constructor() {}

  ngOnChanges(): void {
    if (this.users && this.users.length === 0) {
      this.usersNotFound = true;
    }
    this.usersCopy = Object.assign([], this.users);
    if (this.showPagenation) {
      this.getUsersPerPage(1, 10);
    }
  }

  ngOnInit(): void {}

  changeNumberOfPages(value: number): void {
    this.numberOfRecords = value;
    this.disablePrev = true;
    this.thisPage = 0;
    this.selectedPage = 0;
    this.paginationList = Array(5)
      .fill(0)
      .map((x, i) => i);
    this.getUsersPerPage(1, value);
  }

  getUsersPerPage(startVal: number, index: number, records?: number): void {
    records = records ? records : this.numberOfRecords;
    this.numberOfRecords = records;
    this.selectedPage = index;
    if (this.users) {
      this.usersCopy = this.users.slice(
        (startVal - 1) * records,
        startVal * records
      );
      this.recordsNotFound = this.users.length > 0 ? false : true;
      this.totalPages = Math.ceil(this.users.length / records);
      this.prevNextButtonStatus();
    }
  }

  getNextPrevPages(isNextPage: boolean): void {
    this.thisPage = isNextPage ? this.thisPage + 1 : this.thisPage - 1;
    this.paginationList = Array(5)
      .fill(0)
      .map((x, i) => {
        return i + 5 * this.thisPage;
      });

    this.selectedPage = 0;
    this.startIndex = this.paginationList[0];
    this.lastIndex = this.paginationList[this.paginationList.length - 1];

    this.getUsersPerPage(this.paginationList[0] + 1, this.numberOfRecords);
    this.prevNextButtonStatus();
  }

  prevNextButtonStatus(): void {
    this.disablePrev = this.thisPage === 0 ? true : false;
    this.disableNext =
      this.totalPages <= this.paginationList[this.paginationList.length - 1] + 1
        ? true
        : false;
  }

  onSubmit(id: string, status: string): void {
    console.log('Submit: ');
  }
}
