import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TableComponent } from './table.component';

describe('TableComponent', () => {
  let component: TableComponent;
  let fixture: ComponentFixture<TableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [TableComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('changeNumberOfPages : ', () => {
    it('validatin when the input value is 10 ', () => {
      component.changeNumberOfPages(10);
      expect(component.numberOfRecords).toEqual(10);
      expect(component.disablePrev).toBeTruthy();
      expect(component.paginationList).toEqual([0, 1, 2, 3, 4]);
    });

    it('validatin when the input value is 20 ', () => {
      component.changeNumberOfPages(20);
      expect(component.numberOfRecords).toEqual(20);
      expect(component.disablePrev).toBeTruthy();
      expect(component.paginationList).toEqual([0, 1, 2, 3, 4]);
    });
  });

  describe('getUsersPerPage : ', () => {
    it(`validatin when the start value:  0, index: 0 and records 10`, () => {
      component.getUsersPerPage(0, 0, 10);
      expect(component.numberOfRecords).toEqual(10);
      expect(component.totalPages).toEqual(0);
    });

    it(`validatin when the start value:  20, index: 1 and records 20`, () => {
      component.getUsersPerPage(0, 0, 20);
      expect(component.numberOfRecords).toEqual(20);
      expect(component.totalPages).toEqual(0);
    });
  });

  describe('getNextPrevPages : ', () => {
    it(`validatin when clicked on next page`, () => {
      component.getNextPrevPages(true);

      expect(component.thisPage).toEqual(1);
      expect(component.paginationList).toEqual([5, 6, 7, 8, 9]);
      expect(component.startIndex).toEqual(5);
      expect(component.lastIndex).toEqual(9);

      component.getNextPrevPages(false);

      expect(component.thisPage).toEqual(0);
      expect(component.paginationList).toEqual([0, 1, 2, 3, 4]);
      expect(component.startIndex).toEqual(0);
      expect(component.lastIndex).toEqual(4);
    });
  });

  describe('prevNextButtonStatus : ', () => {
    it(`validatin previous and next buttons on load.`, () => {
      component.prevNextButtonStatus();
      expect(component.disablePrev).toBeTruthy();
      expect(component.disableNext).toBeTruthy();
    });
  });
});
