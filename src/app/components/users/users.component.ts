import { Component, OnInit, OnDestroy } from '@angular/core';
import { User } from 'src/app/models/user.model';
import { UserService } from 'src/app/services/user.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss'],
})
export class UsersComponent implements OnInit, OnDestroy {
  private subscriptions: Subscription[] = [];
  users!: User[];
  showPagenation = true;

  constructor(private userSvc: UserService) {}

  ngOnInit(): void {
    const sub = this.userSvc.getUsers().subscribe((res) => {
      this.users = res;
    });

    this.subscriptions.push(sub);
  }

  togglePagination(pagenation: boolean): void {
    this.showPagenation = pagenation;
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach((s) => s.unsubscribe());
  }
}
