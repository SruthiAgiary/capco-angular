import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UsersComponent } from './users.component';
import { UserService } from 'src/app/services/user.service';
import { of } from 'rxjs';
import { mockUsers } from 'src/app/mock.data';
import { TableComponent } from '../table/table.component';

class MockUserService {
  getUsers() {
    return of(mockUsers);
  }
}
describe('UsersComponent', () => {
  let component: UsersComponent;
  let fixture: ComponentFixture<UsersComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [UsersComponent, TableComponent],
      providers: [{ provide: UserService, useClass: MockUserService }],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UsersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('togglePagination : ', () => {
    it('showPagenation false ', () => {
      component.togglePagination(false);
      expect(component.showPagenation).toBeFalsy();
    });

    it('showPagenation true ', () => {
      component.togglePagination(true);
      expect(component.showPagenation).toBeTruthy();
    });
  });
});
