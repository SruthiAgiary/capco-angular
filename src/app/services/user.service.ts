import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { User } from '../models/user.model';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  constructor(private httpClient: HttpClient) {}

  getUsers(): Observable<User[]> {
    return this.httpClient.get<User[]>('../../../assets/users.json');
  }

  postUser(id: string, status: string): Observable<any> {
    const body = { id, status };
    return this.httpClient.post<any>(`/api/submit/${id}`, body);
  }
}
