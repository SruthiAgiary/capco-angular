import { TestBed } from '@angular/core/testing';
import {
  HttpClientTestingModule,
  HttpTestingController,
} from '@angular/common/http/testing';

import { UserService } from './user.service';

describe('UserService', () => {
  let service: UserService;
  let httpTestingCtrl: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [UserService],
    });
    service = TestBed.inject(UserService);
    httpTestingCtrl = TestBed.inject(HttpTestingController);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  afterEach(() => {
    httpTestingCtrl.verify();
  });

  describe('getUsers : ', () => {
    it('Should the request and response match.', (done) => {
      service.getUsers().subscribe((users) => {
        expect(users).toEqual([]);
        done();
      });

      const req = httpTestingCtrl.expectOne('../../../assets/users.json');
      expect(req.request.method).toEqual('GET');
      req.flush([]);
    });
  });
});
