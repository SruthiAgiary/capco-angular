export interface User {
  [key: string]: any;
  name: string;
  phone: string;
  email: string;
  company: string;
  dateEntry: string;
  orgNum: string;
  address1: string;
  city: string;
  zip: string;
  geo: string;
  pan: string;
  pin: string;
  id: any;
  status: string;
  fee: string;
  guid: string;
  dateRecent: string;
  dateExit: string;
  dateFirst: string;
  url: string;
}
